<?php
namespace Wsoftpro\CategoryLayerFilter\Controller\Index;

use Magento\Framework\App\Action\Context;

class Delete extends \Magento\Framework\App\Action\Action
{
    /** @var \Magento\Framework\Controller\Result\JsonFactory */
    protected $_resultJsonFactory;

    /** @var \Magento\Customer\Model\Session  */
    protected $_customerSession;

    /**
     * Index constructor.
     * @param Context $context
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     * @param \Magento\Customer\Model\Session $customerSession
     */
    public function __construct(
        Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Customer\Model\Session $customerSession
    )
    {
        $this->_resultJsonFactory = $resultJsonFactory;
        $this->_customerSession = $customerSession;
        parent::__construct($context);
    }

    /**
     * @return bool|\Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\Result\Json|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        try {
            $resultJson = $this->_resultJsonFactory->create();
            $this->_customerSession->unsFilterCategorySize();
            return $resultJson->setData(['response' => true, 'url' => $this->getRequest()->getParam('url')]);
        } catch (\Exception $e) {
            return false;
        }
    }
}