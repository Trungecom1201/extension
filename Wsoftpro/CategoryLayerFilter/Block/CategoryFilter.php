<?php
namespace Wsoftpro\CategoryLayerFilter\Block;

use Magento\Framework\View\Element\Template;

class CategoryFilter extends \Magento\Framework\View\Element\Template
{
    const ENABLE_MODULE = 'category_layer/filter/enable';

    /** @var \Magento\Customer\Model\Session  */
    protected $_customerSession;

    /**
     * CategoryFilter constructor.
     * @param Template\Context $context
     * @param \Magento\Customer\Model\Session $customerSession
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        \Magento\Customer\Model\Session $customerSession,
        array $data = []
    ){
        $this->_customerSession = $customerSession;
        parent::__construct($context, $data);
    }

    /**
     * @return mixed
     */
    public function isEnable(){
        return $this->_scopeConfig->getValue(self::ENABLE_MODULE);
    }

    /**
     * @return string
     */
    public function _toHtml()
    {
        if (!$this->isEnable() || $this->getCustomerSession()->isLoggedIn()){
            return '';
        }
        return parent::_toHtml();
    }

    /**
     * @return \Magento\Customer\Model\Session
     */
    public function getCustomerSession(){
        return $this->_customerSession;
    }

    /**
     * @return mixed
     */
    public function getCategoryFilter(){
        return $this->getCustomerSession()->getFilterCategorySize();
    }
}